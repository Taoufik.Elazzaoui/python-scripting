#! /usr/bien/env python3

# Importer les données

import csv
import sqlite3

data = []
names = []

with open('Table.csv') as tableFile:
    table = csv.DictReader(tableFile, delimiter = ';')
    for row in table:
        data.append(row)

for row in data[1]:
    names.append(row)

# Créer la base de données

conn = sqlite3.connect('ciqual.db')
c = conn.cursor()

c.execute(''' CREATE TABLE Nutrient (id serial primary key, name text); ''')
c.execute(''' CREATE TABLE Food (id integer, name text, grp_id integer, ssgrp_id integer, ssssgrp_id integer); ''')
c.execute(''' CREATE TABLE Groupe (id integer, name text, father_grp_id text); ''')
c.execute(''' CREATE TABLE NutData (id integer, food_id integer, nutrient_id integer, value float); ''')

# Remplir la base

id=1
group_id = []
ssgroup_id = []

for name in names[13:]:
    c.execute('INSERT INTO Nutrient VALUES (?,?)', (id,name))
    id+=1

for i in range(len(data)):
    purchases = (data[i][names[6]],data[i][names[7]],data[i][names[0]],data[i][names[1]],data[i][names[2]])
    c.execute('INSERT INTO Food VALUES (?,?,?,?,?)', purchases)

    group_id.append(data[i][names[0]])
    ssgroup_id.append(data[i][names[1]])

group_id = sorted(set(group_id))
ssgroup_id = sorted(set(ssgroup_id))

indice=0
for i in range(len(group_id)):
    
    purchases = (group_id[i],data[indice][names[3]],'null')
    c.execute('INSERT INTO Groupe VALUES (?,?,?)', purchases)
    
    while indice < len(data)-1 and data[indice][names[0]] == data[indice+1][names[0]]:
        indice+=1
    indice+=1

indice=0
for i in range(len(ssgroup_id)):
    
    purchases = (ssgroup_id[i],data[indice][names[4]],data[indice][names[3]])
    c.execute('INSERT INTO Groupe VALUES (?,?,?)', purchases)
    
    while indice < len(data)-1 and data[indice][names[1]] == data[indice+1][names[1]]:
        indice+=1
    indice+=1

#c.executemany('INSERT INTO NutData VALUES (?,?,?,?)', purchases)

conn.commit()
conn.close()