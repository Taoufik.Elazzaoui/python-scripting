#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import os, sys


def extract(directoryPath):

    # Compiler les expressions pour meilleure efficacité du code
    quote = re.compile('\'.*\'')
    double_quotes = re.compile('\".*\"')

    for root, _, filenames in os.walk(directoryPath): # Parcourir le dossier
        for filename in filenames:                       # Parcourir les fichiers
            try:
                with open(os.path.join(root,filename)) as infile:               # Ouvrir un fichier
                    for line in infile:
                        text=quote.search(line)             # Chercher des apostrophes
                        if text:
                            print(text.group())
                        text=double_quotes.search(line)     # Chercher des guillemets
                        if text:
                            print(text.group())
            except FileNotFoundError:
                print("Fichier non trouvé")
def main():
    extract(sys.argv[1])

if __name__ == '__main__':
    main()

