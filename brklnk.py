#! /usr/bien/env python3

import sys,os,re
from bs4 import BeautifulSoup
import requests
import urllib.parse

def brklnk (url):

    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    links = soup.find_all('a')

    for link in links:
        true_url = urllib.parse.urljoin(url,link.get('href'))
        test = requests.get(true_url)

        if not test:
            print(true_url)
            print(link)

def main():
    brklnk(sys.argv[1])

if __name__ == '__main__' :
    main()
